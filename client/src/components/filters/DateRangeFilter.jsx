import React, { useState } from "react";
import DateRangeIcon from '@mui/icons-material/DateRange';
import DateRangeModal from './DateRangeModal';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

const DateRangeFilter = () => {
  const [showModal, setShowModal] = useState(false);
  const openModal = () => {
    setShowModal(true);
  };
  const handleApply = (startDate, endDate) => {
    return;
  }
  return (
    <Box sx={{ display: 'flex' }}>
      <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">
        <FormLabel>Select Date Range</FormLabel>
        <DateRangeIcon
        onClick={openModal}
        />
        {showModal ? <DateRangeModal setShowModal={setShowModal} handleApply={handleApply}/> : null}
      </FormControl>
    </Box>
  )
}
export default DateRangeFilter;
