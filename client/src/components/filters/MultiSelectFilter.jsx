import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import FormGroup from '@mui/material/FormGroup';
import CheckboxFilter from './CheckboxFilter';

const MultiSelectFilter = (props) => {
  const [selectedFilters, setSelectedFilters] = useState([]);

  useEffect(()=> {
    const queryParams = new URLSearchParams(window.location.search);
    const filterParam = queryParams.get(props.name)
    setSelectedFilters(filterParam || []);
  }, [props.name])

  useEffect(()=>{
    const updateFilters = () => {

      const { protocol, host, pathname } = window.location;
      const queryParams = new URLSearchParams(window.location.search);
      queryParams.delete(props.name);
      const newQueryParam = `${props.name}=${[...selectedFilters]}`
      const newQueryParams = `${queryParams}&${newQueryParam}`
      const firstURL = `${protocol}//${host}${pathname}?${newQueryParam}`
      const newURL = `${protocol}//${host}${pathname}?${newQueryParams}`
      switch (queryParams) {
        case Object.keys(queryParams).length === 0:
          window.history.pushState({ path:firstURL }, '', firstURL);
          break;

        default:
          window.history.pushState({ path:newURL }, '', newURL);
          break;
      }
      let newFilters = props.activeFilters
      newFilters[props.name] = selectedFilters || null;
      props.setActiveFilters(newFilters)
    }
    updateFilters();
  }, [selectedFilters, props])

  return (
    <>
      <FormLabel sx={{margin: "20px"}}>
        {props.label}
      </FormLabel>
      <Box sx={{ display: 'flex', height: '400px', overflow: "scroll" }}>
        <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">

          <FormGroup>
            {props.options?.map((option, id) => {
              return <CheckboxFilter
                key={id}
                filterKey={`${props.id}`}
                id={option.id}
                name={option.name}
                selected={selectedFilters}
                setSelected={setSelectedFilters}
              />
            })}
          </FormGroup>
        </FormControl>
      </Box>
    </>
  )

}

export default MultiSelectFilter;
