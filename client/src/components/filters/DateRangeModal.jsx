import React, { useState, useRef } from 'react';
import ReactDom from "react-dom";
import { DateRangePicker } from 'react-date-range';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import './dateRangeModal.css';

const DateRangeModal = (props) => {
  const [startDate,setStartDate]= useState(new Date());
  const [endDate,setEndDate]= useState(new Date());
  const modalRef = useRef();
  const closeModal = (e) => {
    if (e.target === modalRef.current) {
      props.setShowModal(false);
    }
  }
  const selectionRange = {
    startDate: startDate,
    endDate: endDate,
    key: 'selection',
  }
  const handleSelect = (date) =>{
    setStartDate(date.selection.startDate);
    setEndDate(date.selection.endDate);
  };
  return ReactDom.createPortal(
    <div className="container" ref={modalRef} onClick={closeModal}>
      <div className="dateRangeModal">
        <div className="cancel-btn">
          <button onClick={() => props.setShowModal(false)}>X</button>
        </div>
        <h2>Select Start and End Date</h2>
        <div className="datePicker">
          <DateRangePicker
            ranges={[selectionRange]}
            onChange={handleSelect}
          />
        </div>
        <button
          className="Apply-btn"
          onClick={props.handleApply(startDate, endDate)}
        >Apply Dates!</button>
      </div>
    </div>,
    document.getElementById("portal")
  )
}

export default DateRangeModal;
