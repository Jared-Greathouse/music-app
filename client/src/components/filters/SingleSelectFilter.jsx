import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

const SingleSelectFilter = (props) => {
  const [selectedOption, setSelectedOption] = useState("");

  useEffect(()=> {
    const queryParams = new URLSearchParams(window.location.search);
    const filterParam = queryParams.get(props.name)
    setSelectedOption(filterParam || '');
  }, [props.name])

  useEffect(()=> {
    const updateFilters = () => {

      const { protocol, host, pathname } = window.location;
      const queryParams = new URLSearchParams(window.location.search);
      queryParams.delete(props.name);
      const newQueryParam = `${props.name}=${selectedOption}`
      const newQueryParams = `${queryParams}&${newQueryParam}`
      const firstURL = `${protocol}//${host}${pathname}?${newQueryParam}`
      const newURL = `${protocol}//${host}${pathname}?${newQueryParams}`
      switch (queryParams) {
        case Object.keys(queryParams).length === 0:
          window.history.pushState({ path:firstURL }, '', firstURL);
          break;

        default:
          window.history.pushState({ path:newURL }, '', newURL);
          break;
        }
        let newFilters = props.activeFilters
        newFilters[props.name] = selectedOption || null;
        props.setActiveFilters(newFilters)
    }
    updateFilters();
  }, [selectedOption, props])
  return (
    <Box sx={{ display: 'flex' }}>
      <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">
        <FormLabel> {props.label} </FormLabel>
        <select
          className="form-select"
          onChange={e=>setSelectedOption(e.target.value)}
          value={selectedOption || ''}
          required name="selectedOption"
          id="selectedOption"
        >
          <option value="">Choose {props.label}</option>
          {props.options?.map((option, id) => {
            return (
              <option key={id} value={option.name}>
              {option.name}
              </option>
              );
            })}
        </select>
      </FormControl>
    </Box>
  )
}

export default SingleSelectFilter;
