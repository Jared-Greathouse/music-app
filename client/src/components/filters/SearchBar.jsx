import { useState, useEffect } from 'react';
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import TextField from "@mui/material/TextField";

const SearchBar = (props) => {
  const [value, setValue] = useState("")

  useEffect(()=> {
    const passNewValue = () => {
      let newFilters = props.activeFilters;
      newFilters[props.name] = value;
      props.setActiveFilters(newFilters);
    }
    passNewValue();
  }, [value, props])

  return (
    <form>
      <TextField
        id="search-bar"
        className="text"
        onInput={e=>setValue(e.target.value)}
        label="Filter by Track Title"
        variant="outlined"
        placeholder="Search..."
        size="small"
      />
      <IconButton aria-label="search">
        <SearchIcon style={{ fill: "blue" }} />
      </IconButton>
    </form>
  )
};

export default SearchBar;
