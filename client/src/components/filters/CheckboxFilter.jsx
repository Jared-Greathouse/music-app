import { useState } from 'react';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';


const CheckboxFilter = (props) => {
  const [checked, setChecked] = useState(false);

  const handleChange = () => {
    if (checked === false) props.setSelected([...props.selected, props.name])
    if (checked === true) {
      let newArray = [];
      for (let item of props.selected) {
        if (item !== props.name) {
          newArray.push(item)
        }
      }
      props.setSelected(newArray)
    }
    checked === false ? setChecked(true) : setChecked(false);
  }

  return (
    <FormControlLabel
      control={
        <Checkbox
          checked={checked}
          onChange={handleChange}
          inputProps={{ 'aria-label': 'controlled' }}
        />
      }
      label={props.name}
    />
  )
}

export default CheckboxFilter;
