import React, { useState } from 'react';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

const ListOfPlaylists = (props) => {
  const [playlist, setPlaylist] = useState("");
  // if (playlist !== "") {
  //   props.setSelectedPlaylist(playlist)
  // }
  const handleSelection = (event) => {
    setPlaylist(event.target.value)
    props.setSelectedPlaylist(event.target.value)
  }
  return (
    <Box sx={{ display: 'flex' }}>
      <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">
        <FormLabel>Playlists</FormLabel>
        <select
          className="form-select"
          onChange={handleSelection}
          value={playlist.title}
          required name="playlist"
          id="playlist"
        >
          <option value={playlist}>Choose a Playlist</option>
          {props.playlists?.map((playlist) => {
            return (
              <option key={playlist.id} value={playlist.id}>
              {playlist.title}
              </option>
              );
            })}
        </select>
      </FormControl>
    </Box>
  )
}

export default ListOfPlaylists;
