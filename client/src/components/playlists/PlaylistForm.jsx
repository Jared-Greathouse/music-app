import { useState } from "react";
import host from '../../config'

const PlaylistForm = () => {
  const [createdPlaylist, setCreatedPlaylist] = useState("");
  const handleCreatePlaylist = async () => {
    const data = {
      title: createdPlaylist,
      tracks: []
    };
    const url = `${host}/playlists`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      console.log("Created Playlist!");
      setCreatedPlaylist("");
    }
  };

  return (
    <form>
      <input
        className="add-playlist"
        onChange={(e) => setCreatedPlaylist(e.target.value)}
        type="text"
        placeholder="Add New Playlist"
        value={createdPlaylist}
      />
      <button
        onClick={handleCreatePlaylist}
        className="create-playlist-btn"
        variant="secondary"
      >
        Create
      </button>
    </form>
  );
};

export default PlaylistForm;
