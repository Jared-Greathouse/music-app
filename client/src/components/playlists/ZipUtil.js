import JSZip from 'jszip';

const createZipFile = async (fileMeta, fileAudio) => {
  const zip = new JSZip();
  // Add files to the zip
  const metaData = JSON.stringify(fileMeta)
  const response = await fetch(fileAudio);
  const wavContent = await response.blob();
  zip.file(`${fileMeta.title} metadata.txt`, metaData);
  zip.file(`${fileMeta.title}.wav`, wavContent);
  // Generate the zip file
  const content = await zip.generateAsync({ type: 'blob' });
  // Download the zip file
  const url = window.URL.createObjectURL(content);
  const link = document.createElement('a');
  link.href = url;
  link.setAttribute('download', `${fileAudio}.zip`);
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

export default createZipFile;
