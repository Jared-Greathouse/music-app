import React, { useState, useEffect, useRef } from 'react';
import WaveSurfer from 'wavesurfer.js';
import { FaPlayCircle, FaPauseCircle } from 'react-icons/fa';
import DownloadIcon from '@mui/icons-material/Download';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import AddToPlaylistModal from '../modals/AddToPlaylistModal';
import createZipFile from './ZipUtil';
import './waveform.css';

const Waveform = (props) => {
  const [isPlaying, toggleIsPlaying] = useState(false);
  const containerRef = useRef();
  const waveSurferRef = useRef({
    isPlaying: () => false,
  });
  const [showModal, setShowModal] = useState(false);
  const openAddToPlaylistModal = () => {
    setShowModal(true);
  };
  const {item, trackNumber, setCurrentTrack, currentTrack, audio, playAll} = props;
  useEffect(() => {
    const waveSurfer = WaveSurfer.create({
      container: containerRef.current,
      responsive: true,
      height: 64,
      barWidth: 2,
      barHeight: 1,
      cursorWidth: 0,
    });
    waveSurfer.load(audio);
    waveSurfer.on('ready', () => {
      if (playAll === 'true' && currentTrack === trackNumber) {
        waveSurferRef.current = waveSurfer;
        waveSurfer.play();
      } else if (playAll === 'true') {
        setCurrentTrack(trackNumber);
        waveSurferRef.current = waveSurfer;
        waveSurfer.play()
      } else {
        waveSurferRef.current = waveSurfer;
      }
    });
    waveSurfer.on('finish', () => {
      if (playAll === 'true') {
        toggleIsPlaying(waveSurferRef.current.isPlaying())
        setCurrentTrack((currentTrack + 1) % tracks.length);
      } else {
        toggleIsPlaying(waveSurferRef.current.isPlaying())
      }
    })

    return () => {
      waveSurfer.destroy();
    };
  }, [audio, playAll, currentTrack]);
  return (
    <div className="WaveSurferWrap">
      <div className="top-level-entry">
        <button
          onClick={() => {
            waveSurferRef.current.playPause();
            toggleIsPlaying(waveSurferRef.current.isPlaying());
          }}
          type="button"
          >
          {isPlaying ? <FaPauseCircle size="3em" /> : <FaPlayCircle size="3em" />}
        </button>
        <div>
          {props.item.title}
        </div>
        <div>
          <PlaylistAddIcon
            onClick={openAddToPlaylistModal}
          />
          {showModal ? <AddToPlaylistModal setShowModal={setShowModal} track={item}/> : null}
        </div>

        <div>
          <button>
            <a href={audio} download="Young_Folks.wav">
              <DownloadIcon />
              MP3
            </a>
          </button>
        </div>
        <div>
          <button onClick={() => createZipFile(item, audio)}>
            <DownloadIcon />
            ZIP
          </button>
        </div>
      </div>
      <div className="bottom-level-entry">
        <div ref={containerRef} />
      </div>
    </div>
  );
};

export default Waveform;
