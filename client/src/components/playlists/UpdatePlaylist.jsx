import host from '../../config'

const UpdatePlaylist = async (selectedPlaylist, track) => {
  if (selectedPlaylist.tracks.length > 0){
    const data1 = {
      id: selectedPlaylist.id,
      title: selectedPlaylist.title,
      tracks: [...selectedPlaylist.tracks, track]
    };
    const url = `${host}/playlists/${selectedPlaylist.id}`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(data1),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      console.log("Updated Playlist!");
      //return alert that says playlist was updated
    }
  } else {
  const data2 = {
    id: selectedPlaylist.id,
    tracks: [track]
  }
  const url = `${host}/playlists/${selectedPlaylist.id}`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(data2),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      console.log("Updated Playlist!");
      //return alert that says playlist was updated
    }
  }
};

export default UpdatePlaylist;
