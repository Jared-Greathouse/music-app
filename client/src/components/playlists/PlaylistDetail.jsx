import React, { useState, useMemo, useEffect } from 'react';
import { FaPlayCircle, FaPauseCircle } from 'react-icons/fa';
import DownloadIcon from '@mui/icons-material/Download';
import Pagination from './Pagination';
import Waveform from './waveform';
import createZipFile from './ZipUtil';
import YoungFolks from "../songs/Young_Folks.wav";
import './playlistDetail.css';

let PageSize = 10;
const PlaylistDetail = (props) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [playlistPlaying, setPlaylistPlaying] = useState(false);
  const [currentTrack, setCurrentTrack] = useState(null)
  const tracks = props.dataFiltered;

  useEffect(()=>{
    if (currentPage !== 1) {
      setCurrentPage(1)
    }
  }, [tracks, currentPage])

  const currentTableData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * PageSize;
    const lastPageIndex = firstPageIndex + PageSize;
    return tracks?.slice(firstPageIndex, lastPageIndex);
  }, [currentPage, tracks]);

  if (tracks) {
    return (
      <>
        <div className="titleDiv">
          <button
            onClick={() => {
              playlistPlaying === false ? setCurrentTrack(0) : setCurrentTrack(null)
              setPlaylistPlaying(!playlistPlaying)
            }}
            type="button"
          >
          {playlistPlaying ? <FaPauseCircle size="3em" /> : <FaPlayCircle size="3em" />}
          </button>
          Play All
          <h2 className="playlistTitle">{props.title}</h2>
          <div>
            <button>
              <a href={YoungFolks} download="Young_Folks.wav">
              <DownloadIcon />
              MP3
              </a>
            </button>
          </div>
          <div>
            <button onClick={() => createZipFile(props.tracks, YoungFolks)}>
              <DownloadIcon />
              ZIP
            </button>
          </div>
        </div>
        <ol>
          {currentTableData?.map((item, idx) => {
            return (
              <li key={idx}>
                <Waveform 
                  item={item} 
                  trackNumber={idx} 
                  setCurrentTrack={setCurrentTrack} 
                  currentTrack={currentTrack} 
                  audio={YoungFolks} 
                  playAll={playlistPlaying}/>
              </li>
              );
            })}
        </ol>
        <Pagination
          className="pagination-bar pagination"
          currentPage={currentPage}
          totalCount={tracks.length}
          pageSize={PageSize}
          onPageChange={page => setCurrentPage(page)}
        />
      </>
    )
  } else return <></>
}

export default PlaylistDetail;
