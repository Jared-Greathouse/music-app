import host from "../config.js";
const getTracks = async () => {
  const url = `${host}/tracks`;
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    console.log(data);
  }
};
getTracks();
// export default getTracks;

const createTrack = async () => {
  const data = {
    id: 3,
    title: "I am aware that I am sad!",
    length: "30 seconds",
    genre: ["Progressive Metal", "Emo"],
    model: "model 4",
    datasets: "dataset 8"
  };
  const url = `${host}/tracks`;
  const fetchConfig = {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  };
  const response = await fetch(url, fetchConfig);
  if (response.ok) {
    console.log("Created Chatroom!", data);
  }
}

createTrack();
