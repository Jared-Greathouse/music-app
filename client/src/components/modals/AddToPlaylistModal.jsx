import React, { useState, useEffect, useRef } from 'react';
import ReactDom from "react-dom";
import UpdatePlaylist from '../playlists/UpdatePlaylist';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import host from '../../config';
import './modal.css';
// import './dateRangeModal.css';

const AddToPlaylistModal = (props) => {
  const [playlist, setPlaylist]= useState(0);
  const [playlists, setPlaylists]= useState([]);
  const modalRef = useRef();
  const closeModal = (e) => {
    if (e.target === modalRef.current) {
      props.setShowModal(false);
    }
  }
  useEffect(() => {
    const getData = async () => {
      const url = `${host}/playlists`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setPlaylists(data);
      }
    }
    getData();
  }, [])
  const handleApply = () => {
    const selectedPlaylist = playlists[playlist]
    UpdatePlaylist(selectedPlaylist, props.track)
  }

  return ReactDom.createPortal(
    <div className="container" ref={modalRef} onClick={closeModal}>
      <div className="modal">
        <div className="cancel-btn">
          <button onClick={() => props.setShowModal(false)}>X</button>
        </div>
        <h2>Select Playlist to Add this Track</h2>
        <Box sx={{ display: 'flex' }}>
          <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">
            <FormLabel>Playlists</FormLabel>
            <select
              className="form-select"
              onChange={e => setPlaylist(e.target.value)}
              value={playlist.title}
              required name="playlist"
              id="playlist"
            >
              <option value="">Choose a Playlist</option>
              {playlists?.map(({id, title}, idx) => {
                return (
                  <option key={id} value={idx}>
                  {title}
                  </option>
                  );
                })}
            </select>
          </FormControl>
        </Box>
        <button
          className="Apply-btn"
          onClick={handleApply}
        >Add Track!</button>
      </div>
    </div>,
    document.getElementById("portal")
  )
}

export default AddToPlaylistModal;
