import React, { useState, useEffect } from 'react';
import Modal from './components/modals/Modal';
import PlaylistDetail from './components/playlists/PlaylistDetail';
import SearchBar from './components/filters/SearchBar';
import DateRangeFilter from './components/filters/DateRangeFilter';
import ListOfPlaylists from './components/playlists/ListOfPlaylists';
import PlaylistForm from './components/playlists/PlaylistForm';
import MultiSelectFilter from './components/filters/MultiSelectFilter';
import SingleSelectFilter from './components/filters/SingleSelectFilter';
import host from './config';
import './App.css';
function App() {
  const [selectedPlaylist, setSelectedPlaylist] = useState(1);
  const [activePlaylist, setActivePlaylist] = useState("")
  const [filteredPlaylist, setFilteredPlaylist] = useState([])
  const [filters, setFilters] = useState({});
  const [playlists, setPlaylists] = useState([])
  const [activeFilters, setActiveFilters] = useState({});
  const [showModal, setShowModal] = useState(false);
  const openModal = () => {
    setShowModal(true);
  };
  // const satisfiesRangeFilter = (property, filterMin, filterMax) => {
  //   switch (true) {
  //     case !filterMin && !filterMax: return true;
  //     case !filterMax: return property >= filterMin;
  //     case !filterMin: return property <= filterMax;
  //     default: return property >= filterMin && property <= filterMax;
  //   }
  // }
  const satisfiesSelectFilter = (property, filter) => {
    if (!filter) return true;
    if (typeof filter !== 'string') return property === filter;
    return property.toLowerCase().includes(filter.toLowerCase());
  }
  const satisfiesMultiSelectFilter = (property, filters) => {
    if (!filters?.length) return true;
    return filters.some((filter) => property?.includes(filter));
  }
  useEffect(() => {
    const getPlaylist = async () => {
      const url = `${host}/playlists/${selectedPlaylist}`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setActivePlaylist(data);
      }
    }
    getPlaylist();
  }, [selectedPlaylist])
  useEffect(() => {
    const getFilters = async () => {
      const url = `${host}/db`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setFilters(data.filters);
        setPlaylists(data.playlists)
      }
    }
    getFilters();
  }, [])
  useEffect(() => {
    const filterData = (query, data) => {
      if (!data) return;
      if (!query) return data;
      let filteredData = [];
      for (let track of data) {
        if (
          satisfiesSelectFilter(track.model, query.model)
          && satisfiesSelectFilter(track.title, query.title)
          && satisfiesMultiSelectFilter(track.instruments, query.instruments)
          && satisfiesMultiSelectFilter(track.genres, query.genres)
          && satisfiesMultiSelectFilter(track.datasets, query.datasets)
          // && satisfiesRangeFilter(track.length, query.minLength, query.maxLength)
          // && satisfiesRangeFilter(track.date, query.fromDate, query.toDate)
          )
          {filteredData.push(track)}
        }
      return filteredData
    }
    let filteredTracks = filterData(activeFilters, activePlaylist.tracks)
    setFilteredPlaylist(filteredTracks);
  }, [activeFilters, activePlaylist])
  return (
    <div className="App">
      <div className="topDiv">
        <div className="SearchBar">
          <SearchBar
            name={"title"}
            activeFilters={activeFilters}
            setActiveFilters={setActiveFilters}
          />
        </div>
        <p>
        </p>
        <div className="modalButton">
          <button onClick={openModal}>Generate New Audio</button>
          {showModal ? <Modal setShowModal={setShowModal} filters={filters}/> : null}
        </div>
      </div>
      <div className="bottomDiv">
        <div className="leftDiv">
          <ListOfPlaylists playlists={playlists} setSelectedPlaylist={setSelectedPlaylist}/>
          <PlaylistForm />
          <DateRangeFilter />
          <SingleSelectFilter
            options={filters.lengths}
            label={"Minimum Length"}
            name={"minLength"}
            activeFilters={activeFilters}
            setActiveFilters={setActiveFilters}
          />
          <SingleSelectFilter
            options={filters.lengths}
            label={"Maximum Length"}
            name={"maxLength"}
            activeFilters={activeFilters}
            setActiveFilters={setActiveFilters}
          />
          <SingleSelectFilter
            options={filters.models}
            label={"AI Model"}
            name={"model"}
            activeFilters={activeFilters}
            setActiveFilters={setActiveFilters}
          />
          <MultiSelectFilter
            options={filters.instruments}
            label={"Instruments"}
            name={"instruments"}
            activeFilters={activeFilters}
            setActiveFilters={setActiveFilters}
          />
          <MultiSelectFilter
            options={filters.genres}
            label={"Genres"}
            name={"genres"}
            activeFilters={activeFilters}
            setActiveFilters={setActiveFilters}
          />
          <MultiSelectFilter
            options={filters.datasets}
            label={"Datasets"}
            name={"datasets"}
            activeFilters={activeFilters}
            setActiveFilters={setActiveFilters}
          />
        </div>
        <div className="rightDiv">
          <PlaylistDetail title={activePlaylist.title} playlists={filters.playlists} dataFiltered={filteredPlaylist} />
        </div>
      </div>
    </div>
  );
}
export default App;