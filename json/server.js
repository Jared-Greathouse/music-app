const cors = require("cors");

const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();
const port = process.env.PORT || 8080;

server.use(middlewares);
server.use(router);

const allowedDomains = [
  "https://jared-greathouse.gitlab.io/music-app/",
  "https://jared-greathouse.gitlab.io"
];
const corsOptions = {
  origin: function (origin, callback) {
    if (allowedDomains.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  }
}

server.use(cors(corsOptions));

server.listen(port, () => {
  console.log('JSON Server is running');
});